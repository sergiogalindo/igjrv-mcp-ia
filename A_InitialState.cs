﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using MCP_AI;

namespace CustomAICahurnVavas
{
	class A_InitialState: State {

		private static A_InitialState instance = new A_InitialState();
        public override void Enter(StateMachine obj){
            //Debug.Log("Enter " + this.GetType().Name);            
            obj.agent.animation.CrossFade("idle");          
        }        

        public override void Execute(StateMachine obj){
			AgentState s = obj.controller.GetState();

			Commander comander = obj.commander;
			// Actualizar los objetivos prioritarios
//			comander.refreshTargets(obj.agent);
			// Seleccion del estado adecuado para el objetivo
			obj.ChangeState(A_RoamState.GetInstance());
	
		}


        public override void Exit(StateMachine obj){
            //Debug.Log("Exit " + this.GetType().Name);
        }

		public static A_InitialState GetInstance(){
            return instance;
        }
    }
}
