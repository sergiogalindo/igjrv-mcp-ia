using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCP_AI;

namespace CustomAICahurnVavas
{
	public class Commander : MonoBehaviour
	{
		// Environment
		Environment env;

		/// The chaps are the members of the same team.
		public List<GameObject> chaps;

		/// The foreigners are the members of the enemy team.
		public List<GameObject> foreigners;

		/// The outer doors of each building, to camp around.
		public List<GameObject> outerDoors;

		private List<GameObject> capturePoints;

		// List of the targets for each agent.
		public Dictionary<GameObject, GameObject> targets;
		// List of the found agents by triggers.
		public Dictionary<GameObject, GameObject> agents;
		public List<GameObject> agentList;

		// List of defenders at D1 defenders spawn point.
		private Dictionary<GameObject, GameObject> defendersD1;
		public int defD1;
		// List of attackers at A1 attackers spawn point.
		private Dictionary<GameObject, GameObject> attackersA1;
		public int attA1;

		// Number of the current capture point to be reached by attackers
		private int currentCapturePoint;
		// Pointer to the current capture point to be reached by attackers
		public GameObject currentTarget;

		// Number of detected agents
		public int detectedAgents;

		// Flag to recalculate target
		private bool refreshTargets;

		// Flag to determine the calculations for a rush
		public bool firstMission;

		// Defender spawn areas
		public const int TRIGGERATD1 = 0;
		public const int TRIGGERATD2 = 1;
		public const int TRIGGERATD3 = 2;
		// Attackers spawn areas
		public const int TRIGGERATA1 = 3;
		public const int TRIGGERATA2 = 4;

		// Weights
		private const int WEIGHTSNIPPER = 0;
		private const int WEIGHTEXPLORER = 0;
		private const int WEIGHTSOLDIER = 0;
		private const int WEIGHTSUPPORT = 0;

		// Capture points
		private const int K0CapturePoint = 0;
		private const int K1CapturePoint = 1;
		private const int K2CapturePoint = 2;

		// List of triggers to calculate initial positions
		private GameObject[] spawnPointTriggers;

		// Faction of the team
		Environment.FACTIONS faction;

		/// Initializes the commander object with the environment and the faction.
		public void Init(Environment _env, Environment.FACTIONS fact)
		{
			env = _env;

			if (fact == Environment.FACTIONS.ATTACKER)
			{
				chaps = env.attackers;
				foreigners = env.defenders;
			} else
			{
				chaps = env.defenders;
				foreigners = env.attackers;
			}

			outerDoors = new List<GameObject>();
			capturePoints = new List<GameObject>();

			targets = new Dictionary<GameObject, GameObject>();
			agents = new Dictionary<GameObject, GameObject>();
			defendersD1 = new Dictionary<GameObject, GameObject>();
			attackersA1 = new Dictionary<GameObject, GameObject>();
			agentList = new List<GameObject>();

			outerDoors.Add(GameObject.Find("pisazo_puerta13"));
			outerDoors.Add(GameObject.Find("piso1_puerta3"));

			capturePoints.Add (GameObject.Find("KeyLoc-0"));
			capturePoints.Add (GameObject.Find("KeyLoc-1"));
			capturePoints.Add (GameObject.Find("KeyLoc-2"));


			faction = fact;

			SetupLocators();

			refreshTargets = false;
			firstMission = true;
		}

		void Update()
		{
			// If the current target has been taken...
			if (currentTarget != null && currentTarget.GetComponent<KeyLocProperty>().taken)
			{
				firstMission = false;
				refreshTargets = true;
			}

			if (refreshTargets)
				UpdateAgentsTargets();
		}

		private void SetupLocators()
		{
			
			spawnPointTriggers = new GameObject[5];

			detectedAgents = 0;

			List<string> namesList = new List<string>();
			namesList.Add("SpawnAreaD1");
			namesList.Add("SpawnAreaD2");
			namesList.Add("SpawnAreaD3");
			namesList.Add("SpawnAreaA1");
			namesList.Add("SpawnAreaA2");

			GameObject locator;
			SphereCollider sphereColliderDefenders;
			PresenceTrigger pt;

			currentTarget = capturePoints[0];

			for(int i = 0; i < namesList.Count; i++)
			{
				locator = new GameObject("Locator" + namesList[i]);
				locator.transform.position = GameObject.Find (namesList[i]).transform.position;
				locator.transform.parent = this.transform;
				
				locator.AddComponent<SphereCollider>();
				sphereColliderDefenders = locator.GetComponent<SphereCollider>();
				sphereColliderDefenders.radius = 30f;
				
				locator.AddComponent<PresenceTrigger>();
				pt = locator.GetComponent<PresenceTrigger>();

				pt.Init(this, i);

				sphereColliderDefenders.isTrigger = true;
				
				this.spawnPointTriggers[i] = locator;
			}

		}

		public void AddAgentToStartList (ArrayList param)
		{
			if (param.Count == 2)
				AddAgentToStartList((GameObject) param[0], (int) param[1]);
		}

		public void AddAgentToStartList (GameObject a, int source)
		{
			if (!agents.ContainsKey(a))
			{
				agents.Add(a, a);
				agentList.Add(a);
			}
			if (source == TRIGGERATD1)
			{
				if (!defendersD1.ContainsKey(a))
				{
					defendersD1.Add(a, a);
				}

			} 
			else if (source == TRIGGERATA1)
			{
				if (!attackersA1.ContainsKey(a))
				{
					attackersA1.Add(a, a);
				}
			}

			detectedAgents = agents.Count;

			if (agents.Count == 10)
			{
				foreach (GameObject go in spawnPointTriggers)
				{
					go.GetComponent<Collider>().isTrigger = false;
				}

				defD1 = defendersD1.Count;
				attA1 = attackersA1.Count;

				refreshTargets = true;


			}
		}

		private void UpdateAgentsTargets()
		{

			// If its the first mission
			if (firstMission)
			{
				// First mission for attackers
				if (faction == Environment.FACTIONS.ATTACKER)
				{
					// If most defenders are at D1...
					if (defendersD1.Count >= 3 && defendersD1.Count <= 5)
					{
						// Go to K0 capture point.
						currentTarget = capturePoints[K0CapturePoint];
						currentCapturePoint = 0;
					} 
					// If most defenders are not at D1...
					else
					{
						// Go to K1 capture point.
						currentTarget = capturePoints[K1CapturePoint];
						currentCapturePoint = 1;
					}

					AssignNewTargetToAgents(currentTarget);
				}

								
			}
			else
			{

				if (currentCapturePoint == 0)
					currentCapturePoint = 2;
				else if (currentCapturePoint == 2)
					currentCapturePoint = 1;
				else if (currentCapturePoint == 1)
					currentCapturePoint = 0;

				currentTarget = capturePoints[currentCapturePoint];
				AssignNewTargetToAgents(currentTarget);

			}

			refreshTargets = false;
		}

		// Assign a target to all members of the team
		private void AssignNewTargetToAgents(GameObject target)
		{
			Debug.Log("Modificando targets...");
			foreach(GameObject chap in chaps)
			{
				targets[chap] = target;
			}

			currentTarget = target;
		}

		public GameObject GetAgentTarget(GameObject a)
		{
			if (targets.ContainsKey(a))
				return targets[a];
			else
				return GameObject.Find("Centroid").gameObject;
		}


	}
}

