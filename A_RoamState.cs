﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using MCP_AI;

namespace CustomAICahurnVavas
{
	class A_RoamState: State
	{
        private static A_RoamState instance = new A_RoamState();
       
        public override void Enter(StateMachine obj)
        {
            
            AgentState s = obj.controller.GetState();

//			GameObject t = obj.commander.GetAgentTarget(obj.agent);   
//            obj.controller.SetMovementTarget(t.transform);

            
            //Debug.Log("Enter " + this.GetType().Name + " : " + obj.agent.name + " --> " + obj.controller.GetState().Moving);
            //obj.agent.animation.CrossFade("walk");

          
        }

        public override void Execute(StateMachine obj)
        {
            AgentState s = obj.controller.GetState();

            if (s.CurrentPath != null && s.CurrentPath.IsDone())
            {
                obj.controller.GetState().Moving = true;
            }

//			obj.commander.refreshTargets(obj.agent);
           // s.AttackTarget = GameObject.Find("AttTarget");
            
            GameObject e = obj.FindClosestEnemy();
			GameObject t = obj.controller.GetCommander().GetAgentTarget(obj.agent);
/*			if (obj.controller.role==AgentState.SNIPER){
				GameObject t = GameObject.Find("KeyLoc-2");
				obj.controller.SetMovementTarget(t.transform);
				float d = Vector3.Distance(obj.agent.transform.position, e.transform.position);
				
				//Debug.Log(obj.agent.name + ":: " + d+" to "+t.transform.position);
				int distancia;
				if (obj.controller.role==AgentState.SOLDIER){
					distancia=25;
				}else if(obj.controller.role==AgentState.SNIPER){
					distancia=50;
				}else if(obj.controller.role==AgentState.EXPLORER){
					distancia=15;
				}else{
					distancia=20;
				}
				if (d <= distancia)//obj.controller.weapon.range)
				{
					int layer = 1 << 9;
					if (!Physics.Raycast(obj.agent.transform.position, (t.transform.position-obj.agent.transform.position)/d,d,layer))
					obj.ChangeState(A_AttackState.GetInstance());
				}
			}*/
			obj.controller.SetMovementTarget(t.transform);

            float d = Vector3.Distance(obj.agent.transform.position, e.transform.position);

			//Debug.Log(obj.agent.name + ":: " + d+" to "+t.transform.position);
			//mira si esta dentro de su rango de ataque y si es asi ataca
			int distancia;
			if (obj.controller.role==AgentState.SOLDIER){
				distancia=25;
			}else if(obj.controller.role==AgentState.SNIPER){
				distancia=45;
			}else if(obj.controller.role==AgentState.EXPLORER){
				distancia=15;
			}else{
				distancia=20;
			}
			if (d <= distancia)//obj.controller.weapon.range)
			{
				int layer = 1 << 9;
				if (!Physics.Raycast(obj.agent.transform.position, (t.transform.position-obj.agent.transform.position)/d,d,layer))
				obj.ChangeState(A_AttackState.GetInstance());
			}
           
        }

        public override void Exit(StateMachine obj)
        {
            obj.agent.GetComponent<AgentAI>()._state.Moving = false;
        }

        public static A_RoamState GetInstance()
        {
            return instance ;
        }
    }
}
