﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCP_AI;

namespace CustomAICahurnVavas
{
	public class GroupsController : MonoBehaviour {
		public List<GroupType> _groups;

		// Metodos accesores para los grupos
		public void setGroups(List<GroupType> groups){
			_groups = groups;
		}
		public List<GroupType> getGroups(){
			return _groups;
		}

		// Constructores de clase
		public GroupsController(){
			_groups = new List<GroupType> ();
		}

		// Crea las agrupaciones de los agentes
		public void createGroups(List<GameObject> agents){
			List<GameObject> toAddAgents = new List<GameObject> (agents);
			GameObject a;
			while(toAddAgents.Count > 0){
				toAddAgents = addToGroups(toAddAgents);
			}
		}

		// añade los agentes a los grupos pertinentes, devuelve los agentes no añadidos a ningun grupo
		public List<GameObject> addToGroups(List<GameObject> agents){
			List<GameObject> notAdded = new List<GameObject> ();
			 // Solo si la lista de agentes no esta vacia
			if(agents.Count > 0){
				// TODO
			}
			return notAdded;
		}

		// Devuelve el grupo mas cercano al grupo dado, si no hay grupos devuelve un grupo vacio
		public GroupType getNearestGroup(GroupType g){
			GroupType nearest = new GroupType();
			if(_groups.Count > 0){
				// Se considera el primero como el mas cercano
				nearest = _groups [0];
				Vector3 dist = g.getGroupArea().center - nearest.getGroupArea().center;
				Vector3 newDist;
				// Se recorren todos los grupos
				foreach(GroupType gIter in _groups){
					newDist = gIter.getGroupArea().center - g.getGroupArea().center;
					// Si la distancia entro los grupos es menor se asigna el nuevo grupo
					if(dist.magnitude > newDist.magnitude){
						dist = newDist;
						nearest = gIter;
					}
				}
			}
			return nearest;
		}

		// Devuelve el grupo mas cercano a un objeto
		public GroupType getNearestGroup(GameObject item){
			GroupType nearest = new GroupType();
			if(_groups.Count > 0){
				// Se considera el primero como el mas cercano
				nearest = _groups [0];
				Vector3 dist = item.transform.position - nearest.getGroupArea().center;
				Vector3 newDist;
				// Se recorren todos los grupos
				foreach(GroupType gIter in _groups){
					newDist = gIter.getGroupArea().center - item.transform.position;
					// Si la distancia entro los grupos es menor se asigna el nuevo grupo
					if(dist.magnitude > newDist.magnitude){
						dist = newDist;
						nearest = gIter;
					}
				}
			}
			return nearest;
		}

		// Busca un agente en los grupos si el grupo es vacio es que no econtro ningun grupo que lo contenga
		public GroupType find(GameObject agent){
			GroupType group = null;
			// Esto no es eficiente pero como el maximo de grupos es 5 el impacto es menor
			foreach(GroupType g in _groups){
				if(g.isContained(agent)){
					return g;
				}
			}
			return group;
		}

		void update(){
			// Actualiza todos los grupos
			foreach(GroupType g in _groups){
				g.refreshGroup();
			}
		} 

	}
}