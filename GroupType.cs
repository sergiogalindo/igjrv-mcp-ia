﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCP_AI;

namespace CustomAICahurnVavas
{
	public class GroupType : MonoBehaviour {

		// Lista de agentes del grupo
		private List<GameObject> _agents;
		// Area del grupo
		private GameObject _loc;

		// Accesores para la lista de agentes
		public List<GameObject> getAgents(){
			return _agents;
		}
		public void setAgents(List<GameObject> agents){
			_agents = agents;
		}

		// Accesores para el area del grupo
		public BoxCollider getGroupArea(){
			return _loc.GetComponent<BoxCollider>();
		}
		public void setGrouArea(float size){
			// Solo cambia el tamaño del collider su centro se calcula siempre
			_loc.GetComponent<BoxCollider>().size = (new Vector3(_loc.transform.position.x, _loc.transform.position.y, _loc.transform.position.z)) * size;
		}

		// Crea el grupo vacio
		public GroupType(){
			_agents = new List<GameObject> ();
			_loc = new GameObject ();
			_loc.AddComponent<BoxCollider>();
		}

		// Crea un nuevo grupo a partir de una lista de agentes
		public GroupType(List<GameObject> agents){
			_agents = agents;
			_loc = new GameObject ();
			_loc.AddComponent<BoxCollider>();
			// Solo se calcula el centro del collider si hay agentes en la lista
			if(_agents.Count > 0){
				_loc.GetComponent<BoxCollider>().center = _agents[0].transform.position;
				refreshGroup ();
			}
		}

		// Crea un nuevo grupo a partir de una lista de agentes
		public GroupType(List<GameObject> agents, float size){
			_agents = agents;
			_loc = new GameObject ();
			_loc.AddComponent<BoxCollider>();
			// Solo se calcula el centro del collider si hay agentes en la lista
			if(_agents.Count > 0){
				_loc.GetComponent<BoxCollider>().center = _agents[0].transform.position;
				refreshGroup ();
			}
			setGrouArea (size);
		}

		// Calcula el punto medio entre 2 Vector3
		private Vector3 midOfSegment(Vector3 a1, Vector3 a2){
			Vector3 p = new Vector3();
			if(a1 != null && a2 != null){
				// Valores auxiliares de a1 y a2
				float ax = a1.x;
				float ay = a1.y;
				float az = a1.z;
				
				float a2x = a2.x;
				float a2y = a2.y;
				float a2z = a2.z;
				
				// Calcular el punto medio del segmento
				p = new Vector3( (ax + a2x)/2, (ay + a2y)/2, (az + a2z)/2);
			}else{
				// Si uno de los 2 agentes es null devuelve la posicion del que no lo es
				p = (a1 == null)? a2 : a1;
			}
			return p;
		}

		// Calcula el punto medio entre 2 gameObjects
		private Vector3 midOfSegment(GameObject a1, GameObject a2){
			Vector3 p;
			if(a1 != null && a2 != null){
				// Valores auxiliares de a1 y a2
				p = midOfSegment(a1.transform.position, a2.transform.position);
			}else{
				// Si uno de los 2 agentes es null devuelve la posicion del que no lo es
				p = (a1 == null)? a2.transform.position : a1.transform.position;
			}
			return p;
		}

		// Añade un agente a un grupo
		public bool addToGroup(GameObject agent){
			bool added = false;
			if(isContained(agent)){
				_agents.Add(agent);
				_loc.GetComponent<BoxCollider> ().center = midOfSegment(_loc as GameObject, agent);
				added = true;
			}
			return added;
		}

		// Actualiza el grupo
		public void refreshGroup(){
			refreshAgentsOfGroup ();
			refreshCenterOfGroup ();
		}

		// Actualiza los agentes dentro del grupo y recalcuala 
		public void refreshAgentsOfGroup(){
			// Comprueba que todos los agentes sigan dentro del area del grupo
			foreach(GameObject a in _agents){
				if(!isContained(a)){
					_agents.Remove(a);
				}
			}
		}

		// Actualiza el centro del grupo
		public void refreshCenterOfGroup(){
			Vector3 cm = new Vector3();
			foreach(GameObject a in _agents){
				cm = midOfSegment(cm, a.transform.position);
			}
			_loc.GetComponent<BoxCollider> ().center = cm;
		}

		// Vacia el grupo
		public void clear(){
			_agents.Clear ();
			_loc = new GameObject ();
		}

		// Devuelve si un grupo esta vacio
		public bool isEmpty(){
			return _agents.Count > 0;
		}

		// Devuelve si un elemento esta dentro del area de accion del grupo
		public bool isContained(GameObject item){
			return _loc.GetComponent<BoxCollider>().bounds.Contains(item.transform.position);
		}

		// Devuelve una lista de agentes que estan dentro de los margenes del grupo
		public List<GameObject> areContained(List<GameObject> agents){
			List<GameObject> contained = new List<GameObject> ();
			foreach(GameObject a in agents){
				if(isContained(a)){
					contained.Add(a);
				}
			}
				return contained;
		}

		// Une los miebros de un grupo a otro (solo si no hay conflictos)
		public void mergeGroup(GroupType g){
			foreach(GameObject a in g.getAgents()){
				if(addToGroup(a)){
					// Se se fusiona con este grupo se elimina del otro
					g.getAgents().Remove(a);
				}
			}
			g.refreshCenterOfGroup ();
		}

	}

}