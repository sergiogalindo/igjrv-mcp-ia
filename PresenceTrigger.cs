﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCP_AI;
using CustomAICahurnVavas;

public class PresenceTrigger : MonoBehaviour {

	public int id;

	private Commander commander;

	public Dictionary<GameObject, GameObject> attackers;
	public Dictionary<GameObject, GameObject> defenders;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void Init(Commander command, int _id)
	{
		commander = command;
		id = _id;
	}

	void OnTriggerStay(Collider thing)
	{
		if (thing.tag == "Player")
		{
			AIController controller = thing.gameObject.GetComponent<AgentAI>()._controller;

			ArrayList arr = new ArrayList();
			arr.Add(thing.gameObject);
			arr.Add(this.id);

			commander.SendMessage("AddAgentToStartList", arr, SendMessageOptions.DontRequireReceiver);

				
		}
	}
}
