﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using MCP_AI;

namespace CustomAICahurnVavas
{
	class A_AttackState : State
	{
		private static A_AttackState instance = new A_AttackState();
        public override void Enter(StateMachine obj)
        {
            //Debug.Log("Enter " + this.GetType().Name);
            obj.agent.animation.CrossFade("run");

        }

        public override void Execute(StateMachine obj)
        {
			//aqui objetivo prioritario
            GameObject t = obj.FindClosestEnemy();
            float d = Vector3.Distance(obj.agent.transform.position, t.transform.position);
            if (obj.controller.GetState().AttackTarget == null || obj.controller.GetState().CurrentAttackType == -1)
            {
				obj.controller.SetAttack(obj.controller.prefWeapon);
				obj.controller.SetAttackTarget(t);
            }
			int distancia;
			if (obj.controller.role==AgentState.SOLDIER){
				distancia=25;
			}else if(obj.controller.role==AgentState.SNIPER){
				distancia=45;
			}else if(obj.controller.role==AgentState.EXPLORER){
				distancia=15;
			}else{
				distancia=20;
			}

            if (d < obj.controller.GetState().attackTypes[obj.controller.GetState().CurrentAttackType].range/2.0f)
            {
                obj.controller.GetState().Moving = false;   
            }

			else if (d>distancia)
            {
                obj.controller.SetAttackTarget(null);
                obj.controller.SetAttack(-1);
                obj.ChangeState(A_RoamState.GetInstance());
            }
            
            
        }

        public override void Exit(StateMachine obj)
        {
            //Debug.Log("Exit " + this.GetType().Name);
        }

		public static A_AttackState GetInstance()
        {
            return instance;
        }
	}
}
